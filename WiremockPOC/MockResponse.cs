﻿namespace WiremockPOC
{
    public class MockResponse
    {
        public string msg { get; set; }

        public bool success { get; set; }
    }
}