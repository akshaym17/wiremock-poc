﻿using System;
using Xunit;
using WireMock.Server;
using System.Net.Http;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Generic;
using System.Text;
using WireMock.Matchers;

namespace WiremockPOC
{
    public class WiremockPOCTests
    {
        WireMockServer _server;
        public WiremockPOCTests()
        {
            _server = WireMockServer.Start();
        }

        [Fact]
        public void TestGET()
        {
            _server.Given(Request.Create()
                        .WithPath("/test")
                        .UsingGet())
                    .RespondWith(
                        Response.Create()
                        .WithStatusCode(200)
                        .WithBody("GET!")
                    );

            HttpResponseMessage resp = HttpGetRequest(_server.Urls[0], "/test", null);
            string responseBody = resp.Content.ReadAsStringAsync().Result;

            Assert.Equal(HttpStatusCode.OK, resp.StatusCode);
            Assert.Equal("GET!", responseBody);
        }

        [Fact]
        public void TestGETWithParamsAndJSON()
        {
            _server.Given(Request.Create()
                    .WithPath("/test/json")
                    .WithParam("param1", new ExactMatcher("wiremock"))
                    .UsingGet())
                .RespondWith(
                    Response.Create()
                    .WithStatusCode(200)
                    .WithBody(
                        @"{ ""msg"":""GET!"", ""success"":""true""}"
                ));

            string paramString = "param1=wiremock";
            HttpResponseMessage resp = HttpGetRequest(_server.Urls[0], "/test/json", paramString);
            string responseBody = resp.Content.ReadAsStringAsync().Result;
            var respObject = JsonConvert.DeserializeObject<MockResponse>(responseBody);

            Assert.Equal(HttpStatusCode.OK, resp.StatusCode);
            Assert.Equal("GET!", respObject.msg);
            Assert.True(respObject.success);
        }

        [Fact]
        public void TestPOST()
        {
            _server.Given(Request.Create()
                        .WithPath("/test")
                        .UsingPost())
                    .RespondWith(
                        Response.Create()
                        .WithSuccess()
                        .WithBody("POST!")
                    );

            HttpResponseMessage resp = HttpPostRequest(_server.Urls[0], "/test", null);
            string responseBody = resp.Content.ReadAsStringAsync().Result;

            Assert.Equal(HttpStatusCode.OK, resp.StatusCode);
            Assert.Equal("POST!", responseBody);
        }

        [Fact]
        public void TestPOSTWithParamsAndJSON()
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("input", "valid");

            _server.Given(Request.Create()
                        .WithPath("/test/json")
                        .WithBody(JsonConvert.SerializeObject(param))
                        .UsingPost())
                    .RespondWith(
                        Response.Create()
                        .WithStatusCode(200)
                        .WithBody(
                            @"{ ""msg"":""POST!"", ""success"":""true""}"
                    ));

            HttpResponseMessage resp = HttpPostRequest(_server.Urls[0], "/test/json", param);
            string responseBody = resp.Content.ReadAsStringAsync().Result;
            var respObject = JsonConvert.DeserializeObject<MockResponse>(responseBody);

            Assert.Equal(HttpStatusCode.OK, resp.StatusCode);
            Assert.Equal("POST!", respObject.msg);
            Assert.True(respObject.success);
        }

        /// <summary>
        ///  Mimicking getting something like a JWT
        /// </summary>
        [Fact]
        public void TestGetBase64EncodedString()
        {
            _server.Given(Request.Create()
            .WithPath("/test")
            .UsingGet())
        .RespondWith(
            Response.Create()
            .WithStatusCode(200)
            .WithBody(System.Convert.ToBase64String
                        (System.Text.Encoding.UTF8.GetBytes("Fake Encryption!"))
        ));

            HttpResponseMessage resp = HttpGetRequest(_server.Urls[0], "/test", null);
            string responseBody = resp.Content.ReadAsStringAsync().Result;

            Assert.Equal(HttpStatusCode.OK, resp.StatusCode);
            Assert.Equal("Fake Encryption!", System.Text.Encoding.UTF8.GetString
                            (System.Convert.FromBase64String(responseBody)));
        }

        private HttpResponseMessage HttpGetRequest(string baseUrl, string path, string paramString)
        {
            HttpClient client = new HttpClient();
            UriBuilder builder = new UriBuilder(baseUrl + path);
            builder.Query = paramString;

            HttpResponseMessage response = client.GetAsync(builder.Uri).Result;
            return response;
        }

        private HttpResponseMessage HttpPostRequest(string baseUrl, string path, Dictionary<string, string> param)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            StringContent content = new StringContent(JsonConvert.SerializeObject(param));
            HttpResponseMessage response = client.PostAsync(path, content ).Result;
            return response;
        }
    }
}
